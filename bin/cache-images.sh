#!/usr/bin/env bash

echo "- Clearing and rebuilding cache, standby.. -"

./bin/console cache:clear
./bin/console cache:warmup

echo "- Done. Site is ready to be used. -"

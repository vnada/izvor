
function adjustWindow() {
    $('#scene, .layer.main').height($(window).height());
    $('#scene .col a, #scene .col').height($(window).height()/2);
}

function openContact() {
    $("#contact").fadeOut(500);
    $("#contact-panel").show("slide", { direction: "up" }, 500);
    $("#work").hide(500);
    $("#about").hide(500);
}

function closeContact() {
    $("#contact").fadeIn(500);
    $("#contact-panel").hide("slide", { direction: "up" }, 500);
    $("#about").show(500);
    $("#work").show(500);
}

function openWork() {
    $("#work").fadeOut(500);
    $("#work-panel").show("slide", { direction: "left" }, 500);
    $("#about").hide(500);
    $("#contact").hide(500);
}

function closeWork() {
    $("#work").fadeIn(500);
    $("#work-panel").hide("slide", { direction: "left" }, 500);
    $("#about").show(500);
    $("#contact").show(500);
}

function openAbout() {
    $("#about").fadeOut(500);
    $("#about-panel").show("slide", { direction: "right" }, 500);
    $("#work").hide(500);
    $("#contact").hide(500);
}

function closeAbout() {
    $("#about").fadeIn(500);
    $("#about-panel").hide("slide", { direction: "right" }, 500);
    $("#work").show(500);
    $("#contact").show(500);
}

$(document).ready(function () {
    if($("body #homepage").length) {
        adjustWindow();

        $("#work").css('transform','rotate(270deg)');
        $("#about").css('transform','rotate(90deg)');

        $("#about-panel").hide();
        $("#work-panel").hide();
        $("#contact-panel").hide();
        $("h2").hide();

        var hover = $(".hover");

        hover.hover(function() {
            $(this).find("h2").fadeIn(500);
        });

        hover.mouseleave(function() {
            $(this).find("h2").fadeOut(500);
        });

        new Parallax(document.getElementById('scene'));
    }

    if($("body #project").length) {
        $("#back-button").css('transform','rotate(90deg)');
    }
});

$(window).resize(function() {
    adjustWindow();
});

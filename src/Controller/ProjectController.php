<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\ImageLoading;

/**
 * Project page Controller.
 *
 * Handles the rendering and control of the project section.
 *
 * @package App\Controller
 */
class ProjectController extends AbstractController
{
    /**
     * Main page render function.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homepage()
    {
        return $this->render('homepage/homepage.html.twig');
    }

    /**
     * Project page render function.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @param \App\Service\ImageLoading                 $imageLoader
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function projectHandler(Request $request, ImageLoading $imageLoader)
    {
        $tag = $request->attributes->get('_route');

        $images = $imageLoader->getProjectImages($tag);

        return $this->render('projects/generic.html.twig', ['images' => $images]);
    }
}

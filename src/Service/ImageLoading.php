<?php

namespace App\Service;

use Symfony\Component\Finder\Finder;

/**
 * Image loader service used to assist with loading all stored images for display.
 *
 * @package App\Service
 */
class ImageLoading
{

    private $projectDirectory;

    private $finder;

    /**
     * ImageLoading constructor.
     *
     * @param string $projectDirectory
     *   The base image directory.
     */
    public function __construct($projectDirectory)
    {
        $this->projectDirectory = $projectDirectory;
        $this->finder = Finder::create();
    }

    /**
     * Get the current finder.
     *
     * @return \Symfony\Component\Finder\Finder
     */
    public function getFinder()
    {
        return $this->finder;
    }

    /**
     * Get the base image directory path.
     *
     * @return string
     */
    public function getImageDirectory()
    {
        return $this->projectDirectory;
    }

    /**
     * Get the total number of files processed by the finder.
     *
     * @return int
     */
    public function getNumberOfFiles()
    {
        return iterator_count($this->getFinder());
    }

    /**
     * Retrieve all images from the specified directory.
     *
     * @param string $directory
     *   The targeted directory.
     *
     * @return array
     */
    public function getImages($directory)
    {
        $finder = $this->getFinder();
        $finder->files()->in($directory)->sortByName();

        $images = array();
        foreach ($finder as $file) {
            $images[] = $file->getPathname();
        }

        return $images;
    }

    /**
     * Reset the current finder.
     */
    public function resetFinder()
    {
        $this->finder = Finder::create();
    }

    /**
     * Get all the front layer images.
     *
     * @param string $tag
     *  The tag needed for image directory.
     *
     * @return array
     */
    public function getProjectImages($tag)
    {
        $this->resetFinder();

        return $this->getImages($this->getImageDirectory().$tag);
    }
}
